#ifndef SYMULATOR_H_INCLUDED
#define SYMULATOR_H_INCLUDED

#include <iostream>

class symulator
{
    private:
        double X, Y, Z;
        double V_x, V_y, V_z;
        double Vw_x, Vw_y, Vw_z;
        double K;
        double masa;
        double L;//zakres t
        std::string plik;

    public:
        symulator (double, double, double, double, double, double, double, double, double, double, double, double);
        struct parameter {double k; double M; double Wx; double Wy; double Wz;};
        static int jac (double, const double [], double * , double [], void *);
        static int func (double, const double [], double [], void *);
        int rozwiaz ();

};


#endif // SYMULATOR_H_INCLUDED
